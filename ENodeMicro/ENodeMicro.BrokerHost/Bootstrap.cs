﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using ECommon.Components;
using ECommon.Configurations;
using ECommon.Extensions;
using ECommon.Logging;
using ECommon.Serilog;
using EQueue.Broker;
using EQueue.Configurations;
using ENodeMicro.Infrastructure;
using ECommonConfiguration = ECommon.Configurations.Configuration;
using System.Runtime.InteropServices;
using ECommon.Socketing;

namespace ENodeMicro.BrokerHost
{
    public class Bootstrap
    {
        private static BrokerController _broker;

        public static void Initialize()
        {
            InitializeEQueue();
        }
        public static void Start()
        {
            _broker.Start();
        }
        public static void Stop()
        {
            if (_broker != null)
            {
                _broker.Shutdown();
            }
        }

        private static void InitializeEQueue()
        {
            var loggerFactory = new SerilogLoggerFactory();
            //.AddFileLogger("ECommon", "logs\\ecommon")
            //.AddFileLogger("EQueue", "logs\\equeue")
            //.AddFileLogger("ENode", "logs\\enode");

            ECommonConfiguration
                .Create()
                .UseAutofac()
                .RegisterCommonComponents()
                .UseSerilog(loggerFactory)
                .UseJsonNet()
                .RegisterUnhandledExceptionHandler()
                .RegisterEQueueComponents()
                .BuildContainer();
            // 默认是win平台配置
            string storePath = "E:\\ENodeMicro-equeue-store";
            // 如果是Linux
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
            {
                storePath = "/app/ENodeMicro-equeue-store";
            }
            // 如果是OSX平台
            else if (RuntimeInformation.IsOSPlatform(OSPlatform.OSX))
            {
                storePath = "/Users/vanzheng/equeue-stores/ENodeMicro-equeue-store";
            }

            var address = RuntimeInformation.IsOSPlatform(OSPlatform.Linux) ? SocketUtils.GetLocalIPV4() : IPAddress.Loopback;
            var nameServerEndpoint = new IPEndPoint(address, ConfigSettings.NameServerPort);
            var nameServerEndpoints = new List<IPEndPoint> { nameServerEndpoint };
            var brokerSetting = new BrokerSetting(false, storePath)
            {
                NameServerList = nameServerEndpoints
            };
            brokerSetting.BrokerInfo.ProducerAddress = new IPEndPoint(address, ConfigSettings.BrokerProducerPort).ToAddress();
            brokerSetting.BrokerInfo.ConsumerAddress = new IPEndPoint(address, ConfigSettings.BrokerConsumerPort).ToAddress();
            brokerSetting.BrokerInfo.AdminAddress = new IPEndPoint(address, ConfigSettings.BrokerAdminPort).ToAddress();
            _broker = BrokerController.Create(brokerSetting);
            ObjectContainer.Resolve<ILoggerFactory>().Create(typeof(Bootstrap).FullName).Info("Broker initialized.");
        }
    }
}
