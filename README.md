# ENode微服务框架

#### 介绍
# 基于ENode封装的微服务框架

#### 软件架构
软件架构说明

#### 运行步骤
0. 准备环境
> 下载Consul https://www.consul.io/
```bash
// 运行Consul
consul agent -dev
```

1. 进入ENodeMicro文件夹，运行mq组件
```bash
// 运行NameServerHost
make nss
// 运行BrokerHost
make broker
// 运行AdminWeb
make admin
```

2. 运行Agent服务
> 进入ENodeMicro.Agent文件夹
```bash
// 1. 修改mysql连接字符串
// 2. 创建框架数据库, 文件:mysql_enode.sql,数据库名:agent_enode
// 3. 创建业务数据库, 数据库名:agent_data
// 4. 生成还原业务数据库脚本
make mig name=init
// 5. 更新业务数据库
make udb
// 6. 运行EventHost
make event
// 7. 运行CommandHost
make command
// 8. 运行WebAPI
make webapi
```

3. 运行Member服务
> 进入ENodeMicro.Member文件夹
```bash
// 1. 修改mysql连接字符串
// 2. 创建框架数据库, 文件:mysql_enode.sql,数据库名:member_enode
// 3. 创建业务数据库, 数据库名:member_data
// 4. 生成还原业务数据库脚本
make mig name=init
// 5. 更新业务数据库
make udb
// 6. 运行EventHost
make event
// 7. 运行CommandHost
make command
// 8. 运行WebAPI
make webapi
```

#### 安装教程

1.  micro
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
