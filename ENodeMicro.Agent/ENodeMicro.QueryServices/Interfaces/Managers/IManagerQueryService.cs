using System.Threading.Tasks;
using ENodeMicro.Common;
using ENodeMicro.QueryServices.Models.Managers;

namespace ENodeMicro.QueryServices.Interfaces.Managers
{
    /// <summary>
    /// 经营者查询服务接口
    /// </summary>
    public interface IManagerQueryService
    {
        Task<APIResult<Manager>> GetDetailAsync(string id);
    }
}