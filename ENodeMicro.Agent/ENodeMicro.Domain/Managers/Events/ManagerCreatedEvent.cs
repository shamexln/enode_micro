using ENode.Eventing;

namespace ENodeMicro.Domain.Managers.Events
{
    /// <summary>
    /// 经营者创建成功事件
    /// </summary>
    public class ManagerCreatedEvent : DomainEvent<string>
    {
        /// <summary>
        /// 姓名
        /// </summary>
        /// <value></value>
        public string Name { get; private set; }
        /// <summary>
        /// 手机号
        /// </summary>
        /// <value></value>
        public string PhoneNumber { get; private set; }

        private ManagerCreatedEvent() { }

        public ManagerCreatedEvent(string name, string phoneNumber)
        {
            Name = name;
            PhoneNumber = phoneNumber;
        }
    }
}