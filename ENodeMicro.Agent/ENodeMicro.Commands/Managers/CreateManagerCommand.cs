using ENode.Commanding;

namespace ENodeMicro.Commands.Managers
{
    /// <summary>
    /// 创建经营者命令
    /// </summary>
    public class CreateManagerCommand : Command
    {
        /// <summary>
        /// 名称
        /// </summary>
        /// <value></value>
        public string Name { get; private set; }
        /// <summary>
        /// 手机号
        /// </summary>
        public string PhoneNumber { get; private set; }

        private CreateManagerCommand() { }
        public CreateManagerCommand(string aggregateRootId, string name, string phoneNumber) : base(aggregateRootId)
        {
            Name = name;
            PhoneNumber = phoneNumber;
        }
    }
}