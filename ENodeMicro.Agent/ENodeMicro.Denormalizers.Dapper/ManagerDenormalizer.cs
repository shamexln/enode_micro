using System.Threading.Tasks;
using ECommon.Components;
using ENode.Messaging;
using ENodeMicro.Common;
using ENodeMicro.Domain.Managers.Events;

namespace ENodeMicro.Denormalizers.Dapper
{
    /// <summary>
    /// 经营者
    /// </summary>
    [Component]
    public class ManagerDenormalizer : AbstractDenormalizer,
        IMessageHandler<ManagerCreatedEvent>
    {
        /// <summary>
        /// 创建
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public async Task HandleAsync(ManagerCreatedEvent message)
        {
            await TryInsertRecordAsync(async connection => await connection.InsertAsync(new
            {
                Id = message.AggregateRootId,

                message.Name,
                message.PhoneNumber,

                CreatedOn = message.Timestamp,
                UpdatedOn = message.Timestamp,
                message.Version
            }, TableNames.Manager));
        }
    }
}