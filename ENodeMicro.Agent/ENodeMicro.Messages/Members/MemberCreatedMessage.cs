using ENode.Messaging;
using ENodeMicro.Common.Enums;

namespace ENodeMicro.Messages.Members
{
    /// <summary>
    /// 会员创建成功消息
    /// </summary>
    public class MemberCreatedMessage : ApplicationMessage
    {
        /// <summary>
        /// 会员ID
        /// </summary>
        public string MemberId { get; set; }
        /// <summary>
        /// 昵称
        /// </summary>
        public string Nickname { get; set; }
        /// <summary>
        /// 手机号
        /// </summary>
        public string PhoneNumber { get; set; }
        /// <summary>
        /// 性别
        /// </summary>
        public Gender Gender { get; set; }
        /// <summary>
        /// 头像
        /// </summary>
        public string AvatarUrl { get; set; }

        public MemberCreatedMessage() { }

        public MemberCreatedMessage(string memberId, string nickname, string phoneNumber, Gender gender, string avatarUrl)
        {
            MemberId = memberId;
            Nickname = nickname;
            PhoneNumber = phoneNumber;
            Gender = gender;
            AvatarUrl = avatarUrl;
        }
    }
}