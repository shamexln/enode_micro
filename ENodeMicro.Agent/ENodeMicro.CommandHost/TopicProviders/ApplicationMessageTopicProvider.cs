using ENode.EQueue;
using ENode.Messaging;
using ENodeMicro.Messages.Members;

namespace ENodeMicro.CommandHost.TopicProviders
{
    /// <summary>
    /// 应用消息
    /// </summary>
    public class ApplicationMessageTopicProvider : AbstractTopicProvider<IApplicationMessage>
    {
        public ApplicationMessageTopicProvider()
        {
            // RegisterTopic("MemberApplicationMessageTopic", typeof(MemberCreatedMessage));
        }
    }
}