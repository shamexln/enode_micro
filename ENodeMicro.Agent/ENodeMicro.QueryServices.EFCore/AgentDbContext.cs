using ENodeMicro.Common;
using ENodeMicro.QueryServices.Models.Managers;
using Microsoft.EntityFrameworkCore;

namespace ENodeMicro.QueryServices.EFCore
{
    public class AgentDbContext : DbContext
    {
        public AgentDbContext(DbContextOptions<AgentDbContext> options) : base(options)
        {
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Manager>().ToTable(TableNames.Manager);
        }
    }
}