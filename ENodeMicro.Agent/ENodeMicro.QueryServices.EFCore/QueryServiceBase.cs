using System;
using System.Linq;
using System.Threading.Tasks;
using ENodeMicro.Common;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace ENodeMicro.QueryServices.EFCore
{
    /// <summary>
    /// 基类
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class QueryServiceBase<T> where T : ModelBase
    {
        private readonly DbSet<T> _entities;

        public QueryServiceBase(DbContext context)
        {
            _entities = context.Set<T>();
        }
        /// <summary>
        /// 查询对象
        /// </summary>
        /// <returns></returns>
        public IQueryable<T> Query()
        {
            return _entities.AsTracking();
        }
        /// <summary>
        /// 查询对象不跟踪
        /// </summary>
        /// <returns></returns>
        public IQueryable<T> QueryNoTracking()
        {
            return _entities.AsNoTracking();
        }
        /// <summary>
        /// [异步]查询服务结果
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="func">需要执行的方法</param>
        /// <param name="onError">发生错误时触发的方法</param>
        /// <returns></returns>
        protected async Task<APIResult<TResult>> QueryResultAsync<TResult>(Func<IQueryable<T>, Task<APIResult<TResult>>> func, Action<string> onError = null) where TResult : class
        {
            var rsp = new APIResult<TResult>();
            try
            {
                var result = await func(_entities.AsNoTracking());
                rsp.StatusCode = result.StatusCode; // 状态码
                rsp.Message = result.Message; // 信息
                rsp.Success = result.Success; // 是否成功
                rsp.Data = result.Data; // 数据集
            }
            catch (Exception ex)
            {
                rsp.StatusCode = 500;
                rsp.Message = ex.Message;
                onError?.Invoke(JsonConvert.SerializeObject(ex.InnerException));
            }

            return rsp;
        }


        /// <summary>
        /// 查询服务结果
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="func">需要执行的方法</param>
        /// <param name="onError">发生错误时触发的方法</param>
        /// <returns></returns>
        protected APIResult<TResult> QueryResult<TResult>(Func<IQueryable<T>, APIResult<TResult>> func, Action<string> onError = null) where TResult : class
        {
            var rsp = new APIResult<TResult>();
            try
            {
                var result = func(_entities.AsNoTracking());
                rsp.StatusCode = result.StatusCode;// 状态码
                rsp.Message = result.Message; // 信息
                rsp.Success = result.Success; // 是否成功
                rsp.Data = result.Data; // 数据集
            }
            catch (Exception ex)
            {
                rsp.StatusCode = 500;
                rsp.Message = ex.Message;
                onError?.Invoke(JsonConvert.SerializeObject(ex.InnerException));
            }
            return rsp;
        }
    }
}