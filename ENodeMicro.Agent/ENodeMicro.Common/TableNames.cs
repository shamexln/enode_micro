namespace ENodeMicro.Common
{
    /// <summary>
    /// 数据库表名
    /// </summary>
    public static class TableNames
    {
        /// <summary>
        /// 经营者
        /// </summary>
        public const string Manager = "Managers";
    }
}