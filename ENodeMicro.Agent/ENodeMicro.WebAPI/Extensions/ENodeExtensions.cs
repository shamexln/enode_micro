using System.Collections.Generic;
using System.Net;
using System.Reflection;
using ENode.Commanding;
using ENode.Configurations;
using ENode.EQueue;
using EQueue.Clients.Producers;
using EQueue.Configurations;
using ENodeMicro.Common;

namespace ENodeMicro.WebAPI.Extensions
{
    public static class ENodeExtensions
    {
        private static CommandService _commandService;

        public static ENodeConfiguration UseEQueue(this ENodeConfiguration eNodeConfiguration)
        {
            var assemblies = new[] { Assembly.GetExecutingAssembly() };
            eNodeConfiguration.RegisterTopicProviders(assemblies);

            var configuration = eNodeConfiguration.GetCommonConfiguration();
            configuration.RegisterEQueueComponents();

            _commandService = new CommandService();
            configuration.SetDefault<ICommandService, CommandService>(_commandService);

            return eNodeConfiguration;
        }
        public static ENodeConfiguration StartEQueue(this ENodeConfiguration enodeConfiguration)
        {
            var nameServerEndpoint = new IPEndPoint(IPAddress.Loopback, ConfigSettings.NameServerPort);
            var nameServerEndpoints = new List<IPEndPoint> { nameServerEndpoint };
            var commandResultProcessor = new CommandResultProcessor().Initialize(new IPEndPoint(IPAddress.Loopback, 9001));

            _commandService.InitializeEQueue(commandResultProcessor, new ProducerSetting
            {
                NameServerList = nameServerEndpoints
            });

            _commandService.Start();

            return enodeConfiguration;
        }
    }
}