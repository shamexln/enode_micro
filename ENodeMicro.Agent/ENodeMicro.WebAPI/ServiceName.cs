namespace ENodeMicro.WebAPI
{
    /// <summary>
    /// 微服务名称
    /// </summary>
    public static class ServiceName
    {
        /// <summary>
        /// 会员模块服务
        /// </summary>
        public const string MemberModuleGRpcService = "MemberModuleGRpcService";
    }
}