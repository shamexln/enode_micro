using System.Threading.Tasks;
using ENodeMicro.GRPC;
using ENodeMicro.QueryServices.Interfaces.Managers;
using Grpc.Core;
using Microsoft.Extensions.Logging;

namespace ENodeMicro.WebAPI.Services
{
    public class ManagerService:Manager.ManagerBase
    {
        private readonly ILogger<ManagerService> _logger;
        private readonly IManagerQueryService _queryService;

        public ManagerService(ILogger<ManagerService> logger,IManagerQueryService queryService)
        {
            _logger = logger;
            _queryService = queryService;
        }

        public override async Task<GetManagerResponse> GetManager(GetManagerRequest request, ServerCallContext context)
        {
            var res = await _queryService.GetDetailAsync(request.Id);
            var rsp = new GetManagerResponse
            {
                PhoneNumber = res.Data?.PhoneNumber??"没有手机号哦",
                Name = res.Data?.Name??"没有数据哦"
            };
            return rsp;
        }
    }
}