using ECommon.Components;
using ENode.Commanding;
using ENode.EQueue;

namespace ENodeMicro.CommandHost.TopicProviders
{
    [Component]
    public class CommandTopicProvider : AbstractTopicProvider<ICommand>
    {
        public CommandTopicProvider()
        {
            // RegisterTopic("PostCommandTopic", typeof(AcceptNewReplyCommand));
        }
    }
}