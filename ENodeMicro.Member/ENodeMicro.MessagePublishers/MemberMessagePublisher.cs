using System.Threading.Tasks;
using ECommon.Components;
using ENode.Messaging;
using ENodeMicro.Domain.Members.Events;
using ENodeMicro.Messages.Members;

namespace ENodeMicro.MessagePublishers
{
    /// <summary>
    /// 会员消息发布者
    /// </summary>
    [Component]
    public class MemberMessagePublisher
        : IMessageHandler<MemberCreatedEvent>
    {
        private readonly IMessagePublisher<IApplicationMessage> _publisher;

        public MemberMessagePublisher(IMessagePublisher<IApplicationMessage> publisher)
        {
            _publisher = publisher;
        }

        /// <summary>
        /// 会员创建成功事件
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public async Task HandleAsync(MemberCreatedEvent message)
        {
            // 发布会员创建成功的消息
            await _publisher.PublishAsync(new MemberCreatedMessage(
                message.AggregateRootId,
                message.Nickname,
                message.PhoneNumber,
                message.Gender,
                message.AvatarUrl));
        }
    }
}