using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ENodeMicro.GRPC;
using ENodeMicro.QueryServices.Interfaces.Members;
using Grpc.Core;
using Microsoft.Extensions.Logging;

namespace ENodeMicro.WebAPI.Services
{
    public class MemberService:Member.MemberBase
    {
        private readonly ILogger<MemberService> _logger;
        private readonly IMemberQueryService _queryService;

        public MemberService(ILogger<MemberService> logger,IMemberQueryService queryService)
        {
            _logger = logger;
            _queryService = queryService;
        }
        public override async Task<GetMemberResponse> GetMemberByName(GetMemberRequest request, ServerCallContext context)
        {
            var res = await  _queryService.GetMembersAsync();
            
            var rsp = new GetMemberResponse
            {
                Message = res.Message,
                StatusCode = res.StatusCode,
                Success = res.Success,
                Data = { res.Data.Select(p=> new MemberInfo
                {
                    Gender = p.Gender.GetHashCode(),
                    AvatarUrl = p.AvatarUrl,
                    NickName = p.Nickname,
                    PhoneNumber = p.PhoneNumber,
                    Status = p.Status.GetHashCode()
                })}
            };
            return rsp;
        }
    }
}