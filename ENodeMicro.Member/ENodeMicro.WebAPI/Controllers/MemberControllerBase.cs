using ENode.Commanding;
using ENodeMicro.Common;
using Microsoft.AspNetCore.Mvc;

namespace ENodeMicro.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MemberControllerBase : ControllerBase
    {
        /// <summary>
        /// 获取命令API结果
        /// </summary>
        /// <param name="result"></param>
        /// <param name="rsp"></param>
        /// <returns></returns>
        protected APIResult<string> GetCommandApiResult(CommandResult result, APIResult<string> rsp)
        {
            if (result.Status == CommandStatus.Failed)
            {
                rsp.Message = result.Result;
                return rsp;
            }
            rsp.Success = true;
            rsp.Message = "操作成功.";
            rsp.Data = result.AggregateRootId;
            return rsp;
        }
    }
}