namespace ENodeMicro.WebAPI
{
    /// <summary>
    /// 微服务名称
    /// </summary>
    public static class ServiceName
    {
        /// <summary>
        /// 代理商模块服务
        /// </summary>
        public const string AgentModuleGRpcService = "AgentModuleGRpcService";
    }
}