using System;
using ENodeMicro.Common.Enums;

namespace ENodeMicro.QueryServices.Models.Members
{
    /// <summary>
    /// 会员
    /// </summary>
    public class Member : ModelBase
    {
        /// <summary>
        /// 昵称
        /// </summary>
        public string Nickname { get; set; }
        /// <summary>
        /// 手机号
        /// </summary>
        public string PhoneNumber { get; set; }
        /// <summary>
        /// 头像
        /// </summary>
        public string AvatarUrl { get; set; }
        /// <summary>
        /// 性别
        /// </summary>
        public Gender Gender { get; set; }
        /// <summary>
        /// 会员状态
        /// </summary>
        public Status Status { get; set; }
        /// <summary>
        /// 微信OpenId
        /// </summary>
        public string WeChatOpenId { get; set; }
        /// <summary>
        /// 微信小程序OpenId
        /// </summary>
        public string WeChatMpOpenId { get; set; }
        /// <summary>
        /// 微信UnionId
        /// </summary>
        public string WeChatUnionId { get; set; }
        /// <summary>
        /// 支付宝UserID
        ///     预留
        /// </summary>
        public string AlipayUserId { get; set; }
        /// <summary>
        /// 会员等级
        /// </summary>
        public MemberLevel MemberLevel { get; set; }
        /// <summary>
        /// 会员最后一次晋级或保级日期
        /// </summary>
        public DateTime LastGradedTime { get; set; }
        /// <summary>
        /// 经验值
        ///     通过消费获得
        /// </summary>
        public int Experience { get; set; }
        /// <summary>
        /// 积分
        ///     通过消费获得
        /// </summary>
        public int Integral { get; set; }
        /// <summary>
        /// 返点
        ///     通过他人点单DIY订单产生返点值
        /// </summary>
        public int ReturnPoint { get; set; }
        /// <summary>
        /// 会员真实姓名
        /// </summary>
        public string RealName { get; set; }
        /// <summary>
        /// 会员生日
        ///     不可修改
        /// </summary>
        public DateTime? Birthday { get; set; }
        /// <summary>
        /// 会员电子邮箱
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// 会员通讯地址
        /// </summary>
        public string ContactAddress { get; set; }
        /// <summary>
        /// 会员身份证
        /// </summary>
        public string IDCard { get; set; }
    }
}