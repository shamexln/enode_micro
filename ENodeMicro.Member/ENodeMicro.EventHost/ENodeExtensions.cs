using System.Collections.Generic;
using System.Net;
using System.Reflection;
using ENode.Configurations;
using ENode.EQueue;
using EQueue.Clients.Consumers;
using EQueue.Configurations;
using EQueue.Protocols;
using ENodeMicro.Common;

namespace ENodeMicro.EventHost
{
    public static class ENodeExtensions
    {
        private static DomainEventConsumer _eventConsumer;

        public static ENodeConfiguration BuildContainer(this ENodeConfiguration eNodeConfiguration)
        {
            eNodeConfiguration.GetCommonConfiguration().BuildContainer();
            return eNodeConfiguration;
        }
        public static ENodeConfiguration UseEQueue(this ENodeConfiguration eNodeConfiguration)
        {
            var assemblies = new[] { Assembly.GetExecutingAssembly() };
            eNodeConfiguration.RegisterTopicProviders(assemblies);

            var configuration = eNodeConfiguration.GetCommonConfiguration();
            configuration.RegisterEQueueComponents();

            return eNodeConfiguration;
        }
        public static ENodeConfiguration StartEQueue(this ENodeConfiguration eNodeConfiguration)
        {
            var nameServerEndpoint = new IPEndPoint(IPAddress.Loopback, ConfigSettings.NameServerPort);
            var nameServerEndpoints = new List<IPEndPoint> { nameServerEndpoint };

            _eventConsumer = new DomainEventConsumer().InitializeEQueue("MemberEventHostGroup", setting: new ConsumerSetting
            {
                NameServerList = nameServerEndpoints,
                ConsumeFromWhere = ConsumeFromWhere.FirstOffset
            });
            _eventConsumer
                .Subscribe("MemberEventTopic");

            _eventConsumer.Start();

            return eNodeConfiguration;
        }
        public static ENodeConfiguration ShutdownEQueue(this ENodeConfiguration enodeConfiguration)
        {
            _eventConsumer.Shutdown();
            return enodeConfiguration;
        }
    }
}