using ENode.Commanding;

namespace ENodeMicro.Commands.Members
{
    /// <summary>
    /// 使用手机号创建会员命令
    /// </summary>
    public class CreateMemberWithPhoneNumberCommand : Command
    {
        /// <summary>
        /// 手机号
        /// </summary>
        public string PhoneNumber { get; private set; }

        private CreateMemberWithPhoneNumberCommand() { }
        public CreateMemberWithPhoneNumberCommand(string aggregateRootId, string phoneNumber) : base(aggregateRootId)
        {
            PhoneNumber = phoneNumber;
        }
    }
}