using System;

namespace ENodeMicro.Domain.Members
{
    /// <summary>
    /// 会员基本信息
    /// </summary>
    public class MemberBasicInfo
    {
        /// <summary>
        /// 会员真实姓名
        /// </summary>
        public string RealName { get; private set; }
        /// <summary>
        /// 会员生日
        ///     不可修改
        /// </summary>
        public DateTime? Birthday { get; private set; }
        /// <summary>
        /// 会员电子邮箱
        /// </summary>
        public string Email { get; private set; }
        /// <summary>
        /// 会员通讯地址
        /// </summary>
        public string ContactAddress { get; private set; }
        /// <summary>
        /// 会员身份证
        /// </summary>
        public string IDCard { get; private set; }

        public MemberBasicInfo(string realName, DateTime? birthday, string email, string contactAddress, string idCard)
        {
            RealName = realName;
            Birthday = birthday;
            Email = email;
            ContactAddress = contactAddress;
            IDCard = idCard;
        }
    }
}