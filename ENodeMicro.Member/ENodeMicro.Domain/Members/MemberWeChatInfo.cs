namespace ENodeMicro.Domain.Members
{
    /// <summary>
    /// 会员微信相关信息
    /// </summary>
    public class MemberWeChatInfo
    {
        /// <summary>
        /// 微信OpenId
        /// </summary>
        public string WeChatOpenId { get; private set; }
        /// <summary>
        /// 微信小程序OpenId
        /// </summary>
        public string WeChatMpOpenId { get; private set; }
        /// <summary>
        /// 微信UnionId
        /// </summary>
        public string WeChatUnionId { get; private set; }

        public MemberWeChatInfo(string weChatOpenId, string weChatMpOpenId, string weChatUnionId)
        {
            WeChatOpenId = weChatOpenId;
            WeChatMpOpenId = weChatMpOpenId;
            WeChatUnionId = weChatUnionId;
        }
    }
}