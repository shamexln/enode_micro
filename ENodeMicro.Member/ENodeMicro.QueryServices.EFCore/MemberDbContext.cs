using Microsoft.EntityFrameworkCore;

namespace ENodeMicro.QueryServices.EFCore
{
    public class MemberDbContext : DbContext
    {
        public MemberDbContext(DbContextOptions<MemberDbContext> options) : base(options)
        {
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Models.Members.Member>().ToTable("Members");
        }
    }
}