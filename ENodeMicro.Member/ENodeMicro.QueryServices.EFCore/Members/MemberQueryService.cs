using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ECommon.Components;
using ENodeMicro.Common;
using ENodeMicro.QueryServices.Interfaces.Members;
using ENodeMicro.QueryServices.Models.Members;
using Microsoft.EntityFrameworkCore;

namespace ENodeMicro.QueryServices.EFCore.Members
{
    /// <summary>
    /// 会员查询服务接口实现类
    /// </summary>
    [Component]
    public class MemberQueryService : QueryServiceBase<Member>, IMemberQueryService
    {
        public MemberQueryService(MemberDbContext context) : base(context)
        {
        }

        public async Task<APIResult<IList<Member>>> GetMembersAsync()
        {
            return await QueryResultAsync(async query =>
            {
                var rsp = new APIResult<IList<Member>>();
            
                var res = await query.ToListAsync();
                if (res.Count == 0)
                {
                    rsp.Message = "暂无数据.";
                    return rsp;
                }

                rsp.Message = "读取成功.";
                rsp.Data = res;
                rsp.Success = true;
                return rsp;
            });
        }
    }
}