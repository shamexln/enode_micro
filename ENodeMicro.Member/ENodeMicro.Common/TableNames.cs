namespace ENodeMicro.Common
{
    /// <summary>
    /// 数据库表名
    /// </summary>
    public static class TableNames
    {
        /// <summary>
        /// 会员
        /// </summary>
        public const string Member = "Members";
    }
}