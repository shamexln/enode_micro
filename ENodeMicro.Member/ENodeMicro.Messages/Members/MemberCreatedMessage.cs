using ENode.Messaging;
using ENodeMicro.Common.Enums;

namespace ENodeMicro.Messages.Members
{
    /// <summary>
    /// 会员创建成功消息
    ///     TODO 此处为了测试跨BC功能，后续需要删除
    /// </summary>
    public class MemberCreatedMessage : ApplicationMessage
    {
        /// <summary>
        /// 会员ID
        /// </summary>
        public string MemberId { get; private set; }
        /// <summary>
        /// 昵称
        /// </summary>
        public string Nickname { get; private set; }
        /// <summary>
        /// 手机号
        /// </summary>
        public string PhoneNumber { get; private set; }
        /// <summary>
        /// 性别
        /// </summary>
        public Gender Gender { get; private set; }
        /// <summary>
        /// 头像
        /// </summary>
        public string AvatarUrl { get; private set; }

        private MemberCreatedMessage() { }

        public MemberCreatedMessage(string memberId, string nickname, string phoneNumber, Gender gender, string avatarUrl)
        {
            MemberId = memberId;
            Nickname = nickname;
            PhoneNumber = phoneNumber;
            Gender = gender;
            AvatarUrl = avatarUrl;
        }
    }
}